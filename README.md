# tech-red-flags
Source: https://twitter.com/inerati/status/1747312846474801618


Red Flags:
- Uses wired Ethernet
- Uses a VPN all the time
- Firefox
- Ultra wide monitor
- Owns a seedbox
- Uses the CLI version of everything
- Steam notifications show up randomly
- thick ass laptop
- gamer chair
- rgb anything
- puts tower pc on the desk itself instead of below it
- giant ass tower in general
- water cooling anything
- literally anything with gamer aesthetics
- needs more than one power strip
- split keyboard
- desktops connects to a TV
- Obnoxiously large desk 
- messy desk
- linux
- emulators on phone
- thinks piracy is some brave badass thing
- knows what nzbget is
- likes gimp
- obsessed with never using their mouse
- loves the thinkpad nipple thing
- hates trackpads
- wants physical keyboards back on phones
- computer doesn’t use secure boot
- libreoffice
- dual booting
- understanding everything on this grid