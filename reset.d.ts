// This fixes certain ambiguous retun types, such as "any" from JSON.parse
//
// @typescript-eslint/no-unsafe-assignment already makes some things safer
// as it complains about fetch() returning any,
// so effectively this also increases convenience
//
// See: https://github.com/total-typescript/ts-reset
import "@total-typescript/ts-reset";
