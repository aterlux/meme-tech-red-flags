module.exports = {

	// -------------------------------------------------------------------------
	// NOTE: Taken from this uni project of mine:
	// https://gitlab.com/web-tech-degree/ria-gamestart/-/blob/master/gameinc-g/.eslintrc.js
	// -------------------------------------------------------------------------

	"env": {
		"es2020": true,
		// I don't think this should be set to node, but it was complaining about no-undef on the module.exports
		// when this was set to browser
		"node": true,
	},

	// Default standards
	"extends": [
		"eslint:recommended",
		"plugin:solid/recommended",
		"google",
		// Import ruleset
		"./.eslint-global-config.cjs",
	],

	"parserOptions": {
		"ecmaVersion": 2020,
		"sourceType": "module",
		// "tsconfigRootDir": __dirname,
		"project": "./tsconfig.json",
	},

	"plugins": [
		"solid",
		"ban",
		"snakecasejs",
	],

	// Global variables so that we don't get screamed at by ESLint
	"globals": {
		// "Atomics": "readonly",
	},


	// -------------------------------------------------------------------------
	// SECTION TypeScript and SolidJS specific
	// -------------------------------------------------------------------------
	"overrides": [{
		// Target only TypeScript files
		"files": [
			"**/*.ts",
			"**/*.tsx",
		],
		"extends": [
			"plugin:@typescript-eslint/eslint-recommended",
			"plugin:@typescript-eslint/recommended",
			// https://typescript-eslint.io/linting/configs/#recommended-requiring-type-checking
			"plugin:@typescript-eslint/recommended-requiring-type-checking",
			// "plugin:@typescript-eslint/strict",
			// Hallelujah: https://www.npmjs.com/package/eslint-plugin-solid
			"plugin:solid/recommended",
			"google",
			// Import ruleset with extra ts rules.
			// I think the global extends here is redundant but idc
			"./.eslint-global-config.cjs",
			"./.eslint-ts-config.cjs",
		],
		"parserOptions": {
			"parser": "@typescript-eslint/parser",
			"ecmaFeatures": {
				"jsx": true,
			},
			"ecmaVersion": 2020,
			"sourceType": "module",
			"project": "./tsconfig.json",
		},
		"plugins": [
			"solid",
			"@typescript-eslint",
		],
		"rules": {
			"camelcase": [
				"off",
			],

			// Allow the use of ts-ignore for one liners
			// "@typescript-eslint/ban-ts-ignore": "off",
		},
	}],
	// -------------------------------------------------------------------------
	// !SECTION TypeScript specific
	// -------------------------------------------------------------------------

};
