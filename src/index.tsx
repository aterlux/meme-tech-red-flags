import {render} from "solid-js/web";
import "./index.css";
import Home from "./pages/Home";

// const root_el = document.getElementById("root") || document.body;
// rootEl ? ( ) : console.error("Couldn't find root element");

render(() => (
	<Home></Home>
), document.body);
//
