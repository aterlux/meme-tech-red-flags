import {For, createSignal} from "solid-js";
import {createStore, produce} from "solid-js/store";
import type {JSXElement} from "solid-js";


export default (): JSXElement => {
	// Can customise ick value on preference
	// *click* noice
	const [card_list, set_card_list] = createStore([
		{id: 0, selected: false, ick_value: 1, text: "Uses wired Ethernet"},
		{id: 1, selected: false, ick_value: 1, text: "Uses a VPN all the time"},
		{id: 2, selected: false, ick_value: 1, text: "Firefox"},
		{id: 3, selected: false, ick_value: 2, text: "Ultra wide monitor"},
		{id: 4, selected: false, ick_value: 4, text: "Owns a seedbox"},
		{id: 5, selected: false, ick_value: 3, text: "Uses the CLI version of everything"},
		{id: 6, selected: false, ick_value: 2, text: "Steam notifications show up randomly"},
		{id: 7, selected: false, ick_value: 1, text: "thick ass laptop"},
		{id: 8, selected: false, ick_value: 3, text: "gamer chair"},
		{id: 9, selected: false, ick_value: 1, text: "rgb anything"},
		{id: 10, selected: false, ick_value: 1, text: "puts tower pc on the desk itself instead of below it"},
		{id: 11, selected: false, ick_value: 3, text: "giant ass tower in general"},
		{id: 12, selected: false, ick_value: 4, text: "water cooling anything"},
		{id: 13, selected: false, ick_value: 2, text: "literally anything with gamer aesthetics"},
		{id: 14, selected: false, ick_value: 1, text: "needs more than one power strip"},
		{id: 15, selected: false, ick_value: 1, text: "split keyboard"},
		{id: 16, selected: false, ick_value: 1, text: "desktops connects to a TV"},
		{id: 17, selected: false, ick_value: 1, text: "Obnoxiously large desk"},
		{id: 18, selected: false, ick_value: 1, text: "messy desk"},
		{id: 19, selected: false, ick_value: 3, text: "linux"},
		{id: 20, selected: false, ick_value: 4, text: "emulators on phone"},
		{id: 21, selected: false, ick_value: 2, text: "thinks piracy is some brave badass thing"},
		{id: 22, selected: false, ick_value: 4, text: "knows what nzbget is"},
		{id: 23, selected: false, ick_value: 2, text: "likes gimp"},
		{id: 24, selected: false, ick_value: 3, text: "obsessed with never using their mouse"},
		{id: 25, selected: false, ick_value: 2, text: "loves the thinkpad nipple thing"},
		{id: 26, selected: false, ick_value: 1, text: "hates trackpads"},
		{id: 27, selected: false, ick_value: 1, text: "wants physical keyboards back on phones"},
		{id: 28, selected: false, ick_value: 1, text: "computer doesn’t use secure boot"},
		{id: 29, selected: false, ick_value: 1, text: "libreoffice"},
		{id: 30, selected: false, ick_value: 1, text: "dual booting"},
		{id: 31, selected: false, ick_value: 10, text: "understanding everything on this grid"},
	]);

	// const [ick_score, set_ick_score] = createSignal(0);

	// toggles custom ick values and having all as 1
	const [use_noice_score, set_use_noice_score] = createSignal(true);


	const highlight_card = (event_div: HTMLDivElement, card_id: number) => {
		// if (event_div.classList.contains("bingo_boop_selected")) {
		// 	set_ick_score(ick_score() - card_ick_val);
		// }
		// else {
		// 	set_ick_score(ick_score() + card_ick_val);
		// }
		event_div.classList.toggle("bingo_boop_selected");
		set_card_list((card) => card.id === card_id, produce((card) => (card.selected = !card.selected)));
	};

	const aggregate_score = (): number => {
		let total_ick = 0;
		// use_noise_score check inside loop would be dumb
		if (use_noice_score()) {
			for (let i = 0; i !== card_list.length; i++) {
				if (card_list[i].selected) {
					total_ick += card_list[i].ick_value;
				}
			}
		}
		else {
			for (let i = 0; i !== card_list.length; i++) {
				if (card_list[i].selected) {
					total_ick += 1;
				}
			}
		}
		return total_ick;
	};

	return (
		<div class="container_std vh100">
			<h1 class="title-centre title-nyan">Bingo!</h1>
			<div class="bingo_grid">
				<For each={card_list}>
					{(card) => {
						return (
							<div class="bingo_boop" onclick={(e) => highlight_card(e.currentTarget, card.id)}>
								{card.text}
							</div>
						);
					}}
				</For>
			</div>
			<h2 class="title-centre">Score: {aggregate_score()}</h2>
			{/* di3, h3, a, not-actually-a-link-but-button. sue me */}
			<div class="title-centre"><h3>
				<a
					class="cursor_pointer"
					onclick={() => set_use_noice_score(!use_noice_score())}>
					use {use_noice_score() ? "not noice" : "noice"} score
				</a>
			</h3>
			</div>
			<div class="title-centre">
				<code>with reference to: <a href="https://twitter.com/inerati/status/1747312846474801618">tweet</a></code>
			</div>
		</div>
	);
};
