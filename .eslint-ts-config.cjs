module.exports = {
	// This file is used because certain TS rules make ESLint
	// complain about there not being a parser for it.
	// All TS rules that don't make ESLint complain still
	// go into .eslint-global-config.cjs
	"rules": {

		// Make sure promise exceptions are handled
		// https://typescript-eslint.io/rules/no-floating-promises/
		"@typescript-eslint/no-floating-promises": "error",
		// https://typescript-eslint.io/rules/no-misused-promises
		"@typescript-eslint/no-misused-promises": "error",


		"@typescript-eslint/naming-convention": [
			"error",
			{ "selector": "default", "format": ["snake_case"] },
			// JSX.Elements must be _snake_case. I wanted them to be PascalCase but I can't
			// find a way to do this without making all functions PascalCase.
			// And JSX won't treat camelCase or regular snake_case as JSX but as HTML,
			// but an underscore prefix does the trick... I'm starting to have regrets lol
			{ "selector": "function", "format": ["snake_case"], "leadingUnderscore": "allow" },
			// Also allow prefix underscore on variables because of JSX components
			{ "selector": "variable", "format": ["snake_case"], "leadingUnderscore": "allow" },
			// types must be prefixed with t_
			{ "selector": "typeLike", "format": ["snake_case"], "prefix": ["t_"] }
		],
	}
}