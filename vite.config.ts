import { defineConfig, loadEnv } from 'vite';
import vue from '@vitejs/plugin-vue';
import solidPlugin from "vite-plugin-solid";

export default ({ mode }) => {
	process.env = {...process.env, ...loadEnv(mode, process.cwd())};

	return defineConfig({
		base: process.env.VITE_ENV === "prod" ? "https://aterlux.com/tech-icks/" : undefined,
		plugins: [
			solidPlugin(),
		],
		build: {
			target: "es2020",
			// minify: false,
		},
	});
}
